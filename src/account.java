class account
{
    int account_id;
    String name;
    double balance;
    
    
    account(int acno,String n, double openbal)
    {
        this.account_id = acno;
        this.name = n;
        this.balance = openbal;
        
    }
    int getaccountnumber()
    {   
        return this.account_id;
    }
    String getname()
    {   
        return this.name;
    }
    double getbalance()
    {
        return this.balance;
    }
    void deposite(double amt)
    {   
        this.balance += amt;
    }
    boolean withdraw(double amt)
    {
        if(this.balance < amt)
        {
            return false;
        }
        this.balance -= amt;
        return true;
    }
    void display()
    {   
        System.out.println("Account: "+this.account_id+","+this.name+","+this.balance);
  
    }
    static int lastaccountnumber = 1000;
    
    account(String n,double openbal)
    {
        this(++lastaccountnumber, n, openbal);
    }
}



