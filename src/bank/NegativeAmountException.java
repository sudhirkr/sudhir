package bank;

public class NegativeAmountException extends Exception
{
    private double   amount;
    private account account;
    
    NegativeAmountException(String msg,double amt,account ac)
    {
        super(msg);
        this.amount = amt;
        this.account = ac;
      
    }
    public double getAmount()
    {   
        return amount;
    }
     public account getAccount()
    {   
        return account;
    }
    public String toString()
    {   
        return super.toString() +";"+amount+":"+account;
    }
}
