package bank;

public class currentaccount extends account
{
    private static final double minimumbalance = 5000;
    private static final double penalty = 100;
    
    public currentaccount(int acno,String n,double openbal) throws NegativeAmountException
    {   
        super(acno,n,openbal);
        
       
    }
    public currentaccount(String n,double openbal) throws NegativeAmountException
    {
        super(n,openbal);
    }
    public boolean withdraw(double amt) throws NegativeAmountException
    {
        if(amt < 0)
       
      
        throw new NegativeAmountException("current account",amt,this);
        
        if(!super.withdraw(amt))
        {
            return false;
        }
        if(this.getbalance() < minimumbalance)
        {
            //this.getbalance() -= penalty;
            new Transaction("Penalty",false,penalty);
        }
        return true;
    }
    

}
