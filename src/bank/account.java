package bank;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public abstract class account implements Comparable<account>
{
    private int account_id;
    private String name;
   // protected double balance;
   private double balance;
  // private Transaction[] passbook = new Transaction[100];
  // private int nextIndexInPassbook = 0;
  private List passbook = new ArrayList();
    
    public class Transaction
    {   
        private Date dateofTransaction = new Date();
        private String naration;
        private boolean credit;
        private double amount;
        
        public Transaction(String n,boolean c,double amt) throws NegativeAmountException
        {
            if(amt < 0)
            {
                throw new NegativeAmountException("Nagative "+(c ? "credit " : "debit "), amt, account.this);
            }
            this.naration = n;
            this.credit = c;
            this.amount = amt ;
            account.this.balance += getNetAmount();
            //account.this.passbook[nextIndexInPassbook++] = this;
            passbook.add(this);
            
        }
        public Date getDateofTransaction()
        {
            return this.dateofTransaction;
        }
        public String getNaration()
        {
            return this.naration;
        }
        public boolean isCredit()
        {
            return this.credit;
        }
        public double getAmount()
        {
            return this.amount;
        }
        public double getNetAmount()
        {   
            return this.credit ? this.amount : -this.amount;
        }
        public String toString()
        {   
            return dateofTransaction+","+naration+","+(credit ? ("0.0,"+amount) : (amount+",0.0"));
        }
    }
    public void printPassbook()
    {
        double runningBalance = 0;
        //Transaction t = null;
        System.out.println("Passbook of "+name+" Account No.:- "+account_id);
       // for(int transactionIndex = 0 ; transactionIndex < nextIndexInPassbook; transactionIndex++) {
        
            for(Object o : passbook)
            {
               // t = passbook[transactionIndex];
                Transaction t = (Transaction)o;
                runningBalance += t.getNetAmount();
                System.out.printf("%tD\t%-20s\t%10.2f\t%10.2f\n",t.getDateofTransaction(),t.getNaration(),
                (t.isCredit() ? 0:t.getAmount()),(t.isCredit()? t.getAmount():0));
                
                //%20.5\t%10.2f\t%10.2f\t%10.2f\n
                   
                //(t.isCredit() ? ("0.0,"+t.getAmount()): (t.getAmount()+",0.0")),t.getNetAmount(),t.getAmount()); 
                                  
                
            }
        System.out.println("End of Passbook");
    }
    public account(int acno,String n, double openbal) throws NegativeAmountException
    {
        /*if(openbal < 0 )
        {
            throw new NegativeAmountException("Negative opening balance",openbal,this);
        }*/
        
        this.account_id = acno;
        this.name = n;
       // this.balance = openbal; 
        new Transaction("opening balance",true,openbal);
        
    }
    public int getaccountnumber()
    {   
        return this.account_id;
    }
    public String getname()
    {   
        return this.name;
    }
    public double getbalance()
    {
        return this.balance;
    }
    public void deposite(double amt) throws NegativeAmountException
    {   
        /* if(amt < 0 )
        {   
            throw new NegativeAmountException("Negative deposit",amt,this);
        }*/
       // this.balance += amt;
        new Transaction("Deposit",true,amt);
    }
    public boolean withdraw(double amt) throws NegativeAmountException 
    {
      /*  if(amt < 0 )
        {   
            throw new NegativeAmountException("Negative withdrawal",amt,this);
        }*/
        if(this.balance < amt)
        {
            return false;
        }
       // this.balance -= amt;
        new Transaction("withdrawal",false,amt);
        return true;
    }
    public void display()
    {   
        System.out.println("Account: "+this.account_id+","+this.name+","+this.balance);
        System.out.println(this);
  
    }
    private static int lastaccountnumber = 1000;
    
    public account(String n,double openbal) throws NegativeAmountException
    {
        this(++lastaccountnumber, n, openbal);
    }
    public String toString()
    {   
       return this.getClass().getName()+";"+this.account_id+","+this.name+","+this.balance;
    }
    public boolean equals(Object obj)
    {
        if(this.getClass() != obj.getClass())
        {
            return false;
        }
        return this.account_id == ((account)obj).account_id;
    }
    public int hashCode()
    {   
        return this.account_id;
    }
    public int compareTo(account ac)
    {
        return this.account_id - ac.account_id;
    }
    
    
    
}



