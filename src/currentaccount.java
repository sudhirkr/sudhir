class currentaccount extends account
{
    static final double minimumbalance = 5000;
    static final double penalty = 100;
    
    currentaccount(int acno,String n,double openbal)
    {   
        super(acno,n,openbal);
    }
    currentaccount(String n,double openbal)
    {
        super(n,openbal);
    }
    boolean withdraw(double amt)
    {
        if(!super.withdraw(amt))
        {
            return false;
        }
        if(this.balance < minimumbalance)
        {
            this.balance -= penalty;
        }
        return true;
    }
    

}
