//package com.classofjava.net;

import java.io.*;
import java.net.*;
public class EchoServer
{
    private ServerSocket board;
    private Socket phone;
    private InputStream receiver;
    private OutputStream transmitter;
    private boolean connected;
   

public EchoServer(int portno) throws IOException
{
    board = new ServerSocket(portno);
    
}
public void getNextConnection() throws IOException
{
    phone = board.accept();
    receiver = phone.getInputStream();
    transmitter = phone.getOutputStream();
    connected = true;
}
public void serviceConnection() throws IOException
{
    if(!connected)
    {
        throw new IOException("Not connected");
    }
    int input = receiver.read();
    while(input != -1)
    {
        System.out.print((char)input);
        transmitter.write(input);
        input = receiver.read();
    }
        transmitter.close();
        receiver.close();
        phone.close();
        connected = false;
 }
public static void main (String[] args) throws IOException
{
    String portNumberString = args[0];
    int portNumber = Integer.parseInt(portNumberString);
    EchoServer server = new EchoServer(portNumber);
    while(true)
    {
        server.getNextConnection();
        server.serviceConnection();
    }
    
}
}

