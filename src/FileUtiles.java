package com.classofjava.io;

import java.io.File;
import java.util.List;
import java.util.ArrayList;

public class FileUtiles
{   
    public static List<File> searchFiles(File dir,String fileName,List<File> results)
    {
        if(!dir.isDirectory())
        {
            return results;
        }
        File searchFile = new File(dir,fileName);
        if(searchFile.exists())
        {
            results.add(searchFile);
        }
        File[] files = dir.listFiles();
        if(files == null)
        {
            files = new File[0];
        }
        for(File file:files)
        {
            if(file.isDirectory())
            {
                searchFiles(file, fileName, results);
            }
        }
        return results;
    }
    public static void main (String[] args)
    {
        String dirName = args[0];
        String fileName = args[1];
        File dirFile = new File(dirName);
        if(!dirFile.isDirectory())
        {
            System.out.println(dirName+"is not a directory");
            System.exit(0);
        }
        List<File> searchResults = new ArrayList<File> ();
        searchFiles(dirFile,fileName,searchResults);
        for (File file : searchResults)
        {
            System.out.println(file.getAbsolutePath());
        } 
    }
}
