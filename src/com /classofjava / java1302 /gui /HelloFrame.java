package com.classofjava.java1302.gui;

import java.awt.*;
import java.awt.geom.*;

public class HelloFrame extends Frame {
    public HelloFrame() {
        super("Hello world");
        this.setBounds(0, 0, 500, 500);
    }
    public void paint(Graphics g) {
        System.out.println("inside paint");
        Graphics2D g2d = (Graphics2D)g;
        AffineTransform transform = g2d.getTransform();
        g2d.rotate(0.5, 100, 100);
        g2d.drawString("Hello world", 100, 100);
        g2d.setTransform(transform);
        g2d.drawString("Hello world", 100, 100);
    }
    public static void main(String[] args) {
        HelloFrame hf = new HelloFrame();
        hf.setVisible(true);
    }
}
