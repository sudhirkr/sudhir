package com.classofjava.java1302.gui;

import java.awt.*;

public class TestContainer {
    public static void main(String[] args) {
        Frame f = new Frame("Test Container");
        f.setLayout(null);
        f.setBounds(0, 0, 600, 600);
        Panel p = new Panel();
        p.setLayout(null);
        p.setBounds(50, 50, 500, 500);
        p.setBackground(Color.cyan);
        f.setBackground(Color.green);
        Button b1 = new Button("Button1");
        b1.setBounds(100, 100, 50, 15);
        f.add(p);
//        f.add(p, "North");
        p.add(b1);
        Button b2 = new Button("OK");
        b2.setBounds(100, 120, 50, 15);
        p.add(b2);
        Button b3 = new Button("Cancel");
        b3.setBounds(100, 140, 50, 15);
        p.add(b3);
//        f.add(new Button("South"), "South");
//        f.add(new Button("East"), "East");
//        f.add(new Button("West"), "West");
        f.setVisible(true);
    }
}
