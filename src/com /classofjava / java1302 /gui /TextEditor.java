package com.classofjava.java1302.gui;

import java.awt.*;
import java.io.*;

public class TextEditor extends Frame {
    private TextArea editorArea = new TextArea();
    private Button newButton = new Button("New");
    private Button openButton = new Button("Open");
    private Button saveButton = new Button("Save");
    private Button saveAsButton = new Button("Save As");
    private Button exitButton = new Button("Exit");
    private TextField statusField = new TextField();
    private TextField helpField = new TextField();
    private TextField clockField = new TextField();

    private File currentFile;

    public TextEditor() {
        super("untitled");
        this.setBounds(0, 0, 600, 600);
        this.setupComponents();
        this.setupMenus();
        this.setupEvents();
        this.setupClock();
    }
    private void setupComponents() {
        Panel toolBar = new Panel();
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(newButton);
        toolBar.add(openButton);
        toolBar.add(saveButton);
        toolBar.add(saveAsButton);
        toolBar.add(exitButton);
        this.add(toolBar, "North");
        Panel statusBar = new Panel();
        statusBar.setLayout(new GridLayout());
        statusBar.add(statusField);
        statusBar.add(helpField);
        statusBar.add(clockField);
        statusField.setEditable(false);
        helpField.setEditable(false);
        clockField.setEditable(false);
        this.add(statusBar, "South");
        this.add(editorArea, "Center");
    }
    private void setupMenus() {
    
    }
    private void setupEvents() {
    
    }
    private void setupClock() {
        Runnable r = new Runnable() {
                public void run() {
                    while(true) {
                        TextEditor.this.clockField.setText(String.format("%tc", System.currentTimeMillis()));
                        try {
                            Thread.sleep(1000);
                        } catch(InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                }
            };
        Thread th = new Thread(r);
        th.setDaemon(true);
        th.start();
    }
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        editor.setVisible(true);
    }
}
